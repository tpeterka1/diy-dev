//---------------------------------------------------------------------------
//
// Example of using DIY to decompose a continous domain into blocks
//
// Tom Peterka
// Argonne National Laboratory
// 9700 S. Cass Ave.
// Argonne, IL 60439
// tpeterka@mcs.anl.gov
//
// (C) 2011 by Argonne National Laboratory.
// See COPYRIGHT in top-level directory.
//
//--------------------------------------------------------------------------
#include <mpi.h>
#include <stdlib.h>
#include <stddef.h>
#include "diy.h"

using namespace std;

//--------------------------------------------------------------------------
//
// main
//
int main(int argc, char **argv) {

  int dim = 3; // number of dimensions in the problem
  int tot_blocks = 8; // total number of blocks
  float data_mins[3] = {0.0, 0.0, 0.0}; // data global minimum extent
  float data_maxs[3] = {10.0, 10.0, 10.0}; // data global maximum extent
  float block_mins[3], block_maxs[3]; // block extents
  int given[3] = {0, 4, 2}; // constraints on blocking (none so far)
  // -x, +x, -y, +y, -z, +z ghost
  float ghost[6] = {0.5, 0.0, 0.0, 0.5, 0.1, 0.1};
  int num_threads = 1; // number of threads DIY can use
  int nblocks; // my local number of blocks
  int rank; // MPI process
  int did; // domain id
  int dim_nblocks[DIY_MAX_DIM];

  // initialize MPI
  if (num_threads > 1) {
    int thread_level; // threading level that MPI implementation provides
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &thread_level);
    assert(thread_level == MPI_THREAD_FUNNELED);
  } else
    MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  // examples don't do any error checking, but real apps should

  // initialize DIY after initializing MPI
  DIY_Init(dim, num_threads, MPI_COMM_WORLD);

  // decompose domain
  did = DIY_Decompose_cont(ROUND_ROBIN_ORDER, tot_blocks, &nblocks,
			   data_mins, data_maxs, ghost, given, 0);

  // print number of blocks in each dimension
  if (rank == 0) {
    DIY_Num_gids_dim(did, dim_nblocks);
    fprintf(stderr, "DIY decomposed domain into [%d %d %d] blocks\n",
	    dim_nblocks[0], dim_nblocks[1], dim_nblocks[2]);
  }

  // print extents of each block
  struct bb_t bb, ng_bb; // block bounds w/ and w/o ghost
  for (int i = 0; i < nblocks; i++) {
    DIY_Block_bounds(did, i, &bb);
    DIY_No_ghost_block_bounds(did, i, &ng_bb);
    fprintf(stderr, "Block bounds w/  ghost for gid %d mins [%.3f %.3f %.3f] "
	    "maxs [%.3f %.3f %.3f]\n", DIY_Gid(did, i),
	    bb.min[0], bb.min[1], bb.min[2], bb.max[0], bb.max[1], bb.max[2]);
    fprintf(stderr, "Block bounds w/o ghost for gid %d mins [%.3f %.3f %.3f] "
	    "maxs [%.3f %.3f %.3f]\n", DIY_Gid(did, i),
	    ng_bb.min[0], ng_bb.min[1], ng_bb.min[2], 
	    ng_bb.max[0], ng_bb.max[1], ng_bb.max[2]);
  }

  // cleanup
  DIY_Finalize();
  MPI_Finalize();

  fflush(stderr);
  if (rank == 0)
    fprintf(stderr, "\n---Completed successfully---\n");
  return 0;

}
