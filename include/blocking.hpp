//---------------------------------------------------------------------------
//
// blocking class
//
// Tom Peterka
// Argonne National Laboratory
// 9700 S. Cass Ave.
// Argonne, IL 60439
// tpeterka@mcs.anl.gov
//
// (C) 2011 by Argonne National Laboratory.
// See COPYRIGHT in top-level directory.
//
//----------------------------------------------------------------------------

#ifndef _BLOCKING
#define _BLOCKING

#include <vector>
#include <mpi.h>
#include <math.h>
#include "diy.h"
#include "assignment.hpp"

using namespace std;

// full tree node
struct kd_node_t {
  bb_t bounds; // bounds of region correponding to this tree node
  int proc; // process to which this node is block is or will be assigned
  int l_child; // array index of left child, -1 = leaf
  int r_child; // array index of right child, -1 = leaf
  int parent; // array index of parent, -1 = root
};

class Blocking {

 public:

  Blocking(int start_b, int did, int dim, int tot_b, int *data_size, 
	   bool share_face, int *ghost, int *given, Assignment *assignment, 
	   MPI_Comm comm);
  Blocking(int start_b, int did, int dim, int tot_b, float *data_mins, 
	   float *data_maxs, float *ghost, int *given, 
	   Assignment *assignment, MPI_Comm comm);
  Blocking(int start_b, int did, int dim, int tot_b, int *gids, bb_t *bounds,
	   Assignment *assignment, MPI_Comm comm);
  ~Blocking();

  void ComputeBlocking(int *given);
  void ComputeContinuousBlocking(int *given);
  int *GetBlockSizes() { return block_size; }
  int *GetLatSizes() { return lat_size; }
  int GetDim() { return dim; }
  int64_t BlockStartsSizes(int lid, int *starts, int *sizes);
  int64_t BlockSizes(int lid, int *sizes);
  void BlockStarts(int lid, int *starts);
  int64_t TotalBlockSize(int lid);
  void BlockBounds(int lid, bb_t *bounds);
  void NoGhostBlockBounds(int lid, bb_t *bounds);
  void GetNeighbors(int lid, vector<struct gb_t>& neighbors, bool wrap);
  void Gid2Indices(int gid, int& i, int& j);
  void Gid2Indices(int gid, int& i, int& j, int& k);
  void Gid2Indices(int gid, int& i, int& j, int& k, int &l);
  int Indices2Gid(int i, int j, bool wrap);
  int Indices2Gid(int i, int j, int k, bool wrap);
  int Indices2Gid(int i, int j, int k, int l, bool wrap);
  bool InTimeBlock(int g, int lid, int tsize, int tb);
  void NumLatBlocks(int *lat_nblocks);
  int Lid2Gid(int lid);
  int Gid2Lid(int gid); // only for gids on this process
  void BuildTree(float *pts, int loc_num_pts, int glo_num_pts,
		 int num_levels, int num_bins);
  int SearchTree(float *pt, int start_node = 0);
  void GetLeaf(int index, leaf_t *leaf);
  int GetLatticeSize(int dim) { return lat_size[dim]; }
  int GetDataSize(int dim) { return data_size[dim]; }
  void TransferPhysicalBlockBounds // added by Zhanping Liu on 07/01/2013 and last updated on 07/02/2013 ZPL
       ( int      nsBlocks, float ** blckMins, float ** blckMaxs,      // to support NON-Cartesian grids
         float ** rBlkMins, float ** rBlkMaxs, int    * st2s_ids ); 
  void AttachPhysicalBlockBounds   // added by Zhanping Liu on 07/01/2013 and last updated on 07/02/2013 ZPL
       ( int      nsBlocks, int   ** blkSizes,                         // to support NON-Cartesian grids
         float ** blkCords, int    * s2st_ids, int    * st2s_ids );

private:

  void FactorDims(int *given);
  void FactorContinuousDims(int *given);
  void ApplyGhost();
  void ApplyContinuousGhost();
  int BinarySearch(int start, int num_vals, int *vals, int target);
  void AddChildren(int parent, int split_dir, float split_frac);

  int did; // domain id
  int dim; // number of dimensions
  int start_b; // starting gid of this domain (num blocks in prior domains)
  int tot_b; // total number of blocks in the domain
  gb_t *blocks; // my local blocks
  bb_l * rbb_l; // no-ghost LOGICAL  bounds of the local blocks (of THIS process): added by Zhanping Liu on 06/28/2013 ZPL
  bb_t * rbb_t; // no-ghost PHYSICAL bounds of my local blocks: 'PHYSICAL' added by Zhanping Liu on 06/28/2013         ZPL
  int nb; // my local number of blocks
  int rank; // my MPI process rank
  int groupsize; // MPI groupsize
  float data_min[DIY_MAX_DIM]; // global data minimum
  float data_max[DIY_MAX_DIM]; // global data maximum
  int data_size[DIY_MAX_DIM]; // number of vertices in each dimension
  int lat_size[DIY_MAX_DIM]; // number of blocks in each dimension
  int block_size[DIY_MAX_DIM]; // size of discrete blocks in each dimension
  float cont_block_size[DIY_MAX_DIM]; // size of continuous blocks in each dim
  MPI_Comm comm; // MPI communicator
  Assignment *assign; // assignment class
  bool share_face; // whether neighboring blocks share a common face or not
  int ghost[2 * DIY_MAX_DIM]; // discrete ghost layer per side
  float cont_ghost[2 * DIY_MAX_DIM]; // continuous ghost layer per side
  vector <kd_node_t> kd_tree; // KD tree implemented as a vector

};

// callback functions are not class members
static void KdTree_MergeHistogram(char **items, int *gids, int num_items, 
				  int *hdr);
static char *KdTree_CreateHistogram(int *hdr);
static void KdTree_DestroyHistogram(void *item);
static void KdTree_CreateHistogramType(void *item, DIY_Datatype *dtype, 
					int *hdr);

#endif
