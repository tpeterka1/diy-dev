//------------------------------------------------------------------------------
//
// communication class
//
// Tom Peterka
// Argonne National Laboratory
// 9700 S. Cass Ave.
// Argonne, IL 60439
// tpeterka@mcs.anl.gov
//
// (C) 2011 by Argonne National Laboratory.
// See COPYRIGHT in top-level directory.
//
//--------------------------------------------------------------------------

#include "comm.hpp"

//--------------------------------------------------------------------------

extern bool dtype_absolute_address; // addresses in current datatype
                                     // are absolute w.r.t. MPI_BOTTOM
                                     // or relative w.r.t. base address

//--------------------------------------------------------------------------
//
// constructor
//
// comm: MPI communicator
//
Comm::Comm(MPI_Comm comm) {

  this->comm = comm;
  MPI_Comm_rank(comm, &rank);
  MPI_Comm_size(comm, &groupsize);

#ifdef _MPI3

  // create a window for RMA sends and receives and allocate memory for it
  MPI_Win_allocate((DIY_RMA_MAX_ITEMS * 4) * sizeof(int), 
		   sizeof(int), MPI_INFO_NULL, comm, &rma_buf, &rma_win);

  // initialize the window
  MPI_Win_lock_all(MPI_MODE_NOCHECK, rma_win);
  for (int i = 0; i < DIY_RMA_MAX_ITEMS; i++) {
    rma_buf[4 * i] = 0; // mark as available (not taken)
    rma_buf[4 * i + 1] = -1;
    rma_buf[4 * i + 2] = -1;
    rma_buf[4 * i + 3] = 0;
  }
  MPI_Win_sync(rma_win);
  MPI_Barrier(comm);

  // tags for requesting and returning the owner process of a block
  // can be negative numbers for RMA, because tags are just data in
  // the window
  diy_req_proc_tag = -2;
  diy_ret_proc_tag = -3;

#else

  // tags for requesting and returning the owner process of a block
  // need to be non-negative because these are actual tags in MPI 
  // 2-sided communication, using the the two highest possible tags
  void *v;
  int flag;
  MPI_Comm_get_attr( MPI_COMM_WORLD, MPI_TAG_UB, &v, &flag );
  assert(flag);
  diy_req_proc_tag = *(int *)v - 1;
  diy_ret_proc_tag = *(int *)v;

#endif

}
//----------------------------------------------------------------------------
//
// destructor
//
Comm::~Comm() {

#ifdef _MPI3

  // frees the window and the memory allocated for it with MPI_Win_allocate
  MPI_Win_unlock_all(rma_win);
  MPI_Win_free(&rma_win);

#endif

}
//----------------------------------------------------------------------------
//
// sends one item to a destination rank
// item: item to be sent
//  specifically, the base address of the displacements in the item's datatype
//  usually the address of the item but could be MPI_BOTTOM if absolute
//  addresses are used in the datatype
// hdr: header containing quantity information
// dest_rank: destination rank
// gid: source or destination gid, depending on how caller wants to use it
//  message gets tagged with gid and gid is retrieved upon receipt
// dtype: MPI datatype of block
//
void Comm::SendItem(char *item, int *hdr, int dest_rank, int gid,
		  MPI_Datatype *dtype) {

  MPI_Request req;
  snd_item si; // sent item

  si.hdr = hdr;
  if (hdr) {
    MPI_Isend(hdr, DIY_MAX_HDR_ELEMENTS, MPI_INT, dest_rank, 2 * gid, comm, 
	      &req);
    si.hreq = req;
    use_header = true;
  }
  else
    use_header = false;
  MPI_Isend(item, 1, *dtype, dest_rank, 2 * gid + 1, comm, &req);

  si.ireq = req;
  snd_items.push_back(si);

}
//----------------------------------------------------------------------------
//
// initiates receiving one item from a source rank
// item is not available until FinishRecvItems is called
//
// src_rank: source rank
// use_header: whether headers are used
//
// returns: pointer to the item
//
void Comm::StartRecvItem(int src_rank, bool use_header) {

  int *hdr = NULL;
  MPI_Request req;
  rcv_hdr rh; // received header

  if (use_header) {
    hdr = new int[DIY_MAX_HDR_ELEMENTS];
    MPI_Irecv(hdr, DIY_MAX_HDR_ELEMENTS, MPI_INT, src_rank, MPI_ANY_TAG, 
	      comm, &req);
    rh.hdr = hdr;
    rh.req = req;
    this->use_header = true;
  }
  else {
    rh.hdr = NULL;
    this->use_header = false;
  }
  rh.proc = src_rank;
  rh.tag = -1;
  rcv_hdrs.push_back(rh);

}
//----------------------------------------------------------------------------
//
// completes item communication for merge
//
// items: array of pointers to items (output)
// gids: gids of received messeages
//   source or destination gid, depending on how caller used it when sending
// procs: sending processes of received messages (output)
// CreateItem: pointer to user-supplied function that takes a header
//   and creates an item, returning a pointer to it
// RecvItemDtype: pointer to user-supplied function
//   that takes an item and creates an MPI datatype for it
//
// side effects: allocates space for the new items
//
// returns: number of received items
//
int Comm::FinishRecvItemsMerge(char **items, int *gids, int *procs,
			       char * (*CreateItem)(int *),
			       void (*RecvItemDtype)(void*, MPI_Datatype*, 
						     int *)) {

  char* (*CreateItemMerge)(int*) = CreateItem;
  char* (*CreateItemSwap)(int*, int) = NULL;
  void (*RecvItemDtypeMerge)(void*, MPI_Datatype*, int *) = RecvItemDtype;
  void (*RecvItemDtypeSwap)(void*, MPI_Datatype*, int) = NULL;

  return FinishRecvItems(items, gids, procs, 0, CreateItemMerge, 
			 CreateItemSwap, RecvItemDtypeMerge, RecvItemDtypeSwap);

}
//----------------------------------------------------------------------------
//
// item communication for merge
//
// items: array of pointers to items (output)
// gids: gids of received messeages
//   source or destination gid, depending on how caller used it when sending
// procs: sending processes of received messages (output)
// wf: wait_factor for nonblocking communication [0.0-1.0]
// 0.0 waits the minimum (1 message per round)
// 1.0 waits the maximum (all messages per round)
// CreateItem: pointer to user-supplied function that takes a header
//   and creates an item, returning a pointer to it
// RecvItemDtype: pointer to user-supplied function
//   that takes an item and creates an MPI datatype for it
//
// side effects: allocates space for the new items
//
// returns: number of received items
//
int Comm::RecvItemsMerge(char **items, int *gids, int *procs, float wf,
			 char * (*CreateItem)(int *),
			 void (*RecvItemDtype)(void*, MPI_Datatype*, int *)) {

  char* (*CreateItemMerge)(int*) = CreateItem;
  char* (*CreateItemSwap)(int*, int) = NULL;
  void (*RecvItemDtypeMerge)(void*, MPI_Datatype*, int *) = RecvItemDtype;
  void (*RecvItemDtypeSwap)(void*, MPI_Datatype*, int) = NULL;

  return RecvItems(items, gids, procs, wf, 0, CreateItemMerge, 
		   CreateItemSwap, RecvItemDtypeMerge, RecvItemDtypeSwap);

}
//----------------------------------------------------------------------------
//
// completes item communication for swap
//
// items: array of pointers to items (output)
// gids: gids of received messeages (output)
//   source or destination gid, depending on how caller used it when sending
// procs: sending processes of received messages (output)
// ne: number of elements in the received item (less than entire item)
// CreateItem: pointer to user-supplied function that takes a header and
//   the current part out of total number of parts, and
//   creates an item, returning a pointer to it
// RecvItemDtype: pointer to user-supplied function
//   that takes an item and creates an MPI datatype for it
//
// side effects: allocates space for the new items
//
// returns: number of received items
//
int Comm::FinishRecvItemsSwap(char **items, int *gids, int *procs, int ne,
			     char * (*CreateItem)(int *, int),
			     void (*RecvItemDtype)(void*, MPI_Datatype*, 
						    int)) {

  char* (*CreateItemMerge)(int*) = NULL;
  char* (*CreateItemSwap)(int*, int) = CreateItem;
  void (*RecvItemDtypeMerge)(void*, MPI_Datatype*, int *) = NULL;
  void (*RecvItemDtypeSwap)(void*, MPI_Datatype*, int) = RecvItemDtype;
  return FinishRecvItems(items, gids, procs, ne, CreateItemMerge, 
			 CreateItemSwap, RecvItemDtypeMerge, RecvItemDtypeSwap);

}
//----------------------------------------------------------------------------
//
// item communication for swap
//
// items: array of pointers to items (output)
// gids: gids of received messeages (output)
//   source or destination gid, depending on how caller used it when sending
// procs: sending processes of received messages (output)
// wf: wait_factor for nonblocking communication [0.0-1.0]
// 0.0 waits the minimum (1 message per round)
// 1.0 waits the maximum (all messages per round)
// ne: number of elements in the received item (less than entire item)
// CreateItem: pointer to user-supplied function that takes a header and
//   the current part out of total number of parts, and
//   creates an item, returning a pointer to it
// RecvItemDtype: pointer to user-supplied function
//   that takes an item and creates an MPI datatype for it
//
// side effects: allocates space for the new items
//
// returns: number of received items
//
int Comm::RecvItemsSwap(char **items, int *gids, int *procs, float wf, int ne,
		      char * (*CreateItem)(int *, int),
		      void (*RecvItemDtype)(void*, MPI_Datatype*, 
					     int)) {

  char* (*CreateItemMerge)(int*) = NULL;
  char* (*CreateItemSwap)(int*, int) = CreateItem;
  void (*RecvItemDtypeMerge)(void*, MPI_Datatype*, int *) = NULL;
  void (*RecvItemDtypeSwap)(void*, MPI_Datatype*, int) = RecvItemDtype;
  return RecvItems(items, gids, procs, wf, ne, CreateItemMerge, 
		   CreateItemSwap, RecvItemDtypeMerge, RecvItemDtypeSwap);

}
//----------------------------------------------------------------------------
//
// completes item communication
//
// items: array of pointers to items (output)
// gids: gids of received messeages (output)
//   source or destination gid, depending on how caller used it when sending
// procs: sending processes of received messages (output)
// ne: number of elements in the received item (less than entire item)
//   used only for swap; pass 0 (or whatever) for mege
// CreateItemMerge: pointer to user-supplied function that takes a header
//   and creates an item, returning a pointer to it
// CreateItemSwap: similar to CreateItemMerge, but function takes two
//  more ints for fraction of total data
// RecvItemDtypeMerge: pointer to user-supplied function
//   that takes an item and creates an MPI datatype for it
//   and returns the base address associated with the datatype
// RecvItemDtypeSwap: similar to RecvItemDtypeMerge, but function takes two
//  more ints for fraction of total data to convert to data type
// Only one of RecvItemDtypeMerge and RecvItemDtypeSwap should be non-null,
//  the other null
//
// side effects: allocates space for the new items
//
// returns: number of received items
//
int Comm::FinishRecvItems(char **items, int *gids, int *procs, int ne,
			  char* (*CreateItemMerge)(int *),
			  char* (*CreateItemSwap)(int *, int),
			  void (*RecvItemDtypeMerge)(void*, MPI_Datatype*, 
						     int *),
			  void (*RecvItemDtypeSwap)(void*, MPI_Datatype*, 
						    int)) {

  MPI_Request req;
  vector<MPI_Request> reqs, reqs1;
  void *addr; // base address for datatype
  MPI_Datatype dm;
  MPI_Status stats[rcv_hdrs.size()];

  // flush completion of header sends
  if (use_header) {
    for (int i = 0; i < (int)snd_items.size(); i++)
      reqs.push_back(snd_items[i].hreq);
    MPI_Waitall((int)reqs.size(), &reqs[0], MPI_STATUS_IGNORE);
  }

  // flush completion of header receives
  if (use_header) {
    reqs.clear();
    for (int i = 0; i < (int)rcv_hdrs.size(); i++)
      reqs.push_back(rcv_hdrs[i].req);
    MPI_Waitall((int)reqs.size(), &reqs[0], stats);
    for (int i = 0; i < (int)rcv_hdrs.size(); i++)
      rcv_hdrs[i].tag = stats[i].MPI_TAG;
  }

  // post item receives
  reqs.clear();
  for (int i = 0; i < (int)rcv_hdrs.size(); i++) {

    if (RecvItemDtypeMerge) { // merge
      items[i] = CreateItemMerge(rcv_hdrs[i].hdr);
      RecvItemDtypeMerge((void *)items[i], &dm, rcv_hdrs[i].hdr);
      if (dtype_absolute_address)
	addr = MPI_BOTTOM;
      else
	addr = items[i];
    }
    else { // swap
      items[i] = CreateItemSwap(rcv_hdrs[i].hdr, ne);
      RecvItemDtypeSwap((void *)items[i], &dm, ne);
      if (dtype_absolute_address)
	addr = MPI_BOTTOM;
      else
	addr = items[i];
    }
    if (use_header) {	
      MPI_Irecv(addr, 1, dm, rcv_hdrs[i].proc, rcv_hdrs[i].tag + 1, comm, &req);
      gids[i] = rcv_hdrs[i].tag / 2;
      procs[i] = rcv_hdrs[i].proc;
      delete[] rcv_hdrs[i].hdr;
    }
    else
      MPI_Irecv(addr, 1, dm, rcv_hdrs[i].proc, MPI_ANY_TAG, comm, &req);
    MPI_Type_free(&dm);
    reqs1.push_back(req);

  }

  // flush completion of item sends
  reqs.clear();
  for (int i = 0; i < (int)snd_items.size(); i++)
    reqs.push_back(snd_items[i].ireq);
  MPI_Waitall((int)reqs.size(), &reqs[0], MPI_STATUS_IGNORE);
  snd_items.clear();

  // flush completion of item receives
  MPI_Waitall((int)reqs1.size(), &reqs1[0], stats);
  if (!use_header) {
    for (int i = 0; i < (int)rcv_hdrs.size(); i++) {
      gids[i] = (stats[i].MPI_TAG - 1) / 2;
      procs[i] = stats[i].MPI_SOURCE;
    }
  }

  rcv_hdrs.clear();

  return (int)reqs1.size();

}
//----------------------------------------------------------------------------
//
// item communication
//
// items: array of pointers to items (output)
// gids: gids of received messeages (output)
//   source or destination gid, depending on how caller used it when sending
// procs: sending processes of received messages (output)
// ne: number of elements in the received item (less than entire item)
//   used only for swap; pass 0 (or whatever) for mege
// CreateItemMerge: pointer to user-supplied function that takes a header
//   and creates an item, returning a pointer to it
// CreateItemSwap: similar to CreateItemMerge, but function takes two
//  more ints for fraction of total data
// RecvItemDtypeMerge: pointer to user-supplied function
//   that takes an item and creates an MPI datatype for it
// RecvItemDtypeSwap: similar to RecvItemDtypeMerge, but function takes two
//  more ints for fraction of total data to convert to data type
// Only one of RecvItemDtypeMerge and RecvItemDtypeSwap should be non-null,
//  the other null
//
// side effects: allocates space for the new items
//
// returns: number of received items
//
int Comm::RecvItems(char **items, int *gids, int *procs, float wf, int ne,
		    char* (*CreateItemMerge)(int *),
		    char* (*CreateItemSwap)(int *, int),
		    void (*RecvItemDtypeMerge)(void*, MPI_Datatype*, int *),
		    void (*RecvItemDtypeSwap)(void*, MPI_Datatype*, 
					       int)) {

  MPI_Request req;
  vector<MPI_Request> reqs, reqs1;
  void *addr; // base address for datatype
  MPI_Datatype dm;
  MPI_Status stats[rcv_hdrs.size()];
  MPI_Status stat;
  int arr[rcv_hdrs.size()]; // requests that arrived
  int narr; // number of requests that arrived
  int tot_narr = 0; // total number counts-receive messages arrived this round
  int min_arr = (int)(wf * (int)rcv_hdrs.size()); // wait for this number of 
                                                  // messsages to arrive
  static bool first = true; // first time

  // post item receives only once if not using headers
  if (!use_header && first) {

    for (int i = 0; i < (int)rcv_hdrs.size(); i++) {

      if (RecvItemDtypeMerge) { // merge
	items[i] = CreateItemMerge(rcv_hdrs[i].hdr);
	RecvItemDtypeMerge((void *)items[i], &dm, rcv_hdrs[i].hdr);
	if (dtype_absolute_address)
	  addr = MPI_BOTTOM;
	else
	  addr = items[i];
      }
      else { // swap
	items[i] = CreateItemSwap(rcv_hdrs[i].hdr, ne);
	RecvItemDtypeSwap((void *)items[i], &dm, ne);
	if (dtype_absolute_address)
	  addr = MPI_BOTTOM;
	else
	  addr = items[i];
      }
      MPI_Irecv(addr, 1, dm, rcv_hdrs[i].proc, MPI_ANY_TAG, comm, &req);
      MPI_Type_free(&dm);
      reqs1.push_back(req);

    }

    first = false;

  }

  while (tot_narr < min_arr) {

    // using header
    if (use_header) {

      // wait for enough headers to arrive
      reqs.clear();
      for (int i = 0; i < (int)rcv_hdrs.size(); i++)
	reqs.push_back(rcv_hdrs[i].req);
      MPI_Waitsome((int)reqs.size(), &reqs[0], &narr, arr, stats);
      for (int i = 0; i < (int)rcv_hdrs.size(); i++)
	rcv_hdrs[i].tag = stats[i].MPI_TAG;

      // receive items
      reqs.clear();
      for (int i = 0; i < (int)rcv_hdrs.size(); i++) {

	if (RecvItemDtypeMerge) { // merge
	  items[i] = CreateItemMerge(rcv_hdrs[i].hdr);
	  RecvItemDtypeMerge((void *)items[i], &dm, rcv_hdrs[i].hdr);
	  if (dtype_absolute_address)
	    addr = MPI_BOTTOM;
	  else
	    addr = items[i];
	}
	else { // swap
	  items[i] = CreateItemSwap(rcv_hdrs[i].hdr, ne);
	  RecvItemDtypeSwap((void *)items[i], &dm, ne);
	  if (dtype_absolute_address)
	    addr = MPI_BOTTOM;
	  else
	    addr = items[i];
	}
	MPI_Recv(addr, 1, dm, rcv_hdrs[i].proc, rcv_hdrs[i].tag + 1, comm, 
		 &stat);
	gids[i] = rcv_hdrs[i].tag / 2;
	procs[i] = rcv_hdrs[i].proc;
	delete[] rcv_hdrs[i].hdr;
	MPI_Type_free(&dm);
	reqs1.push_back(req);

      }

    } // using header

    // no header
    else {

      MPI_Waitsome((int)reqs.size(), &reqs[0], &narr, arr, stats);

    } // no header

  }

  return (int)reqs1.size();

}
//----------------------------------------------------------------------------
//
// sends items (all items in an epoch need to be same datatype)
//
// item: item(s) to be sent
// count: number of items
// datatype: item datatype
// dest_gid: destination gid
// assign: assignment objects
//
void Comm::Send(void *item, int count, DIY_Datatype datatype, 
		int dest_gid, vector <Assignment*> assign) {

  int did; // domain id

  // find the domain of dest_gid
  for (did = 0; did < DIY_Num_dids(); did++) {
    if (DIY_Start_gid(did) <= dest_gid && 
	dest_gid < DIY_Start_gid(did) + DIY_Num_gids(did))
      break;
  }
  assert(did < DIY_Num_dids());
  MPI_Request req;
  int dest_proc = assign[did]->Gid2Proc(dest_gid);
  MPI_Isend(item, count, datatype, dest_proc, dest_gid, comm, &req);
  rma_reqs.push_back(req);

}
//----------------------------------------------------------------------------
//
// receives items (all of the same datatype)
//
// my_gid: my gid
// items: item(s) to be received
// datatype: item datatype
// src_gids: (output) gids of source blocks
// wait: whether to wait for one or more items to arrive (0 or 1)
// sizes: size of each item received in datatypes (not bytes)
//  (output, array allocated by caller)
//
// returns: number of items received
//
int Comm::Recv(int my_gid, void** &items, DIY_Datatype datatype, int wait,
	       int *sizes) {

  MPI_Status status;
  int flag;
  int num_items = 0; // number of items pending for my gid
  int more_items = 0; // maybe more items
  MPI_Aint lb, extent; // datatype lower bound, extent

  // poll as needed according to wait parameter
  while (!num_items || more_items) {

    MPI_Iprobe(MPI_ANY_SOURCE, my_gid, comm, &flag, &status);
    if (flag) {
      int size; // message size in bytes
      MPI_Get_count(&status, MPI_BYTE, &size);
      items[num_items] = new unsigned char[size];
      int src_proc = status.MPI_SOURCE;
      MPI_Recv(items[num_items], size, MPI_BYTE, src_proc, my_gid, comm, 
	       &status);
      MPI_Type_get_extent(datatype, &lb, &extent);
      sizes[num_items] = (int)(size / extent);
      num_items++;
      more_items = 1;

    }
    else
      more_items = 0;

    if (!wait)
      break;

  }

  return num_items;

}
//----------------------------------------------------------------------------
//
// looks up block table
//
// dest_proc: process to query for block table lookup
// gid: the block gid whose owner process is desired
//
// returns: owner process (< 0 if error)
//
int Comm::LookupProc(int dest_proc, int gid) {

  int number_records = 0;
  int owner_proc = 0;
  int unused = 0;

  // starting block in the domain that contains this block
  int i;
  for (i = 0; i < (int)start_gids.size(); i++)
    if (gid < start_gids[i])
      break;

  // local index of the first entry of the block in block table, 
  // record number of records
  int rowNumber = (gid - start_gids[i - 1])  / groupsize;
  int table_lid = rowNumber * tableWidth;

#ifdef _MPI3

  MPI_Fetch_and_op(&unused, &number_records, MPI_INT, dest_proc, table_lid,
  MPI_NO_OP, rma_block_win);
  MPI_Win_flush(dest_proc, rma_block_win);

#else

  MPI_Win_lock(MPI_LOCK_SHARED, dest_proc, MPI_MODE_NOCHECK, rma_block_win);
  MPI_Get(&number_records, 1, MPI_INT, dest_proc, table_lid, 1, MPI_INT,
	  rma_block_win);
  MPI_Win_unlock(dest_proc, rma_block_win);

#endif

  srand(time(NULL));
  int secretNumber = rand() % (number_records);
  secretNumber++;
  table_lid = table_lid+secretNumber;

#ifdef _MPI3

  MPI_Fetch_and_op(&unused, &owner_proc, MPI_INT, dest_proc, table_lid,
  MPI_NO_OP, rma_block_win);
  // apparently necessary, crashes without the flush
  MPI_Win_flush(dest_proc, rma_block_win);

#else

  MPI_Win_lock(MPI_LOCK_SHARED, dest_proc, MPI_MODE_NOCHECK, rma_block_win);
  MPI_Get(&owner_proc, 1, MPI_INT, dest_proc, table_lid, 1, MPI_INT,
	  rma_block_win);
  MPI_Win_unlock(dest_proc, rma_block_win);

#endif
  
  return owner_proc;

}
//----------------------------------------------------------------------------
//
// flushes sends and receives
//
// barrier: whether to issue a barrier (0 or 1)
//   (recommended if more sends and receives to follow)
//
void Comm::FlushSendRecv(int barrier) {

  // flush the nonblocking payload sends
  MPI_Status stats[rma_reqs.size()];
  MPI_Waitall((int)rma_reqs.size(), &rma_reqs[0], stats);
  rma_reqs.clear();

  if (barrier)
    MPI_Barrier(comm);

}
//----------------------------------------------------------------------------

#ifdef _MPI3

//----------------------------------------------------------------------------
//
// sends RMA items (all items in an epoch need to be same datatype)
//
// item: item(s) to be sent
// count: number of items
// datatype: item datatype
// my_gid: my gid
// dest_gid: destination gid
// assign: assignment objects
//
void Comm::RmaSend(void *item, int count, DIY_Datatype datatype, 
		   int my_gid, int dest_gid, vector <Assignment*> assign) {

  int did; // domain id
  MPI_Request req;

  // masg[0] = taken, msg[1] = src gid, msg[2] = dest gid, msg[3] = item count
  int msg[4]; 

  // find the domain of dest_gid
  for (did = 0; did < DIY_Num_dids(); did++) {
    if (DIY_Start_gid(did) <= dest_gid && 
	dest_gid < DIY_Start_gid(did) + DIY_Num_gids(did))
      break;
  }
  assert(did < DIY_Num_dids());
  int dest_proc = assign[did]->Gid2Proc(dest_gid);

  // put the message in an unused slot in the window
  for (int i = 0; i < DIY_RMA_MAX_ITEMS; i++) {

    int taken; // whether item is taken
    int incr = 1; // increment amount
    MPI_Fetch_and_op(&incr, &taken, MPI_INT, dest_proc, 4 * i, 
		     MPI_SUM, rma_win);
    MPI_Win_flush(dest_proc, rma_win);

    if (!taken) { // available slot

      // put the item using accumulate (atomic)
      msg[0] = 1;
      msg[1] = my_gid;
      msg[2] = dest_gid;
      msg[3] = count;
      MPI_Accumulate(msg, 4, MPI_INT, dest_proc, 4 * i, 4, MPI_INT,
		     MPI_REPLACE, rma_win);

      // this flush appears to be necessary, sometimes hangs otherwise
      // todo: confirm
      MPI_Win_flush(dest_proc, rma_win);

      break;

    }

  }

  // send the payload
  MPI_Isend(item, count, datatype, dest_proc, dest_gid, comm, &req);
  rma_reqs.push_back(req);

}
//----------------------------------------------------------------------------
//
// receives pending RMA items (all of the same datatype)
//
// my_gid: my gid
// item: item(s) to be received
// datatype: item datatype
// src_gids: (output) gids of source blocks
// wait: whether to wait for one or more items to arrive (0 or 1)
// assign: assignment class object
// sizes: size of each item received in datatypes (not bytes)
//  (output, array allocated by caller)
//
// returns: number of items received
//
int Comm::RmaRecv(int my_gid, void** &items, DIY_Datatype datatype, 
		  int *src_gids, int wait, Assignment *assign, int *sizes) {

  int loc_rma_buf[4 * DIY_RMA_MAX_ITEMS]; // local copy of rma buffer
  MPI_Status status;
  int num_items = 0; // number of items pending for my gid

  // masg[0] = taken, msg[1] = src gid, msg[2] = dest gid, msg[3] = item count
  int msg[4]; 

  // read the RMA buffer, polling as needed according to wait parameter
  while (!num_items) {

    for (int i = 0; i < DIY_RMA_MAX_ITEMS; i++) {
      int unused[4];
      MPI_Get_accumulate(unused, 4, MPI_INT, msg, 4, MPI_INT, rank, 4 * i, 4,
			 MPI_INT, MPI_NO_OP, rma_win);
      MPI_Win_flush_local(rank, rma_win); // todo: appears to be necessary

      if (msg[0] && msg[2] == my_gid) {

	// save the messsage
	loc_rma_buf[4 * num_items]     = msg[0];
	loc_rma_buf[4 * num_items + 1] = msg[1];
	loc_rma_buf[4 * num_items + 2] = msg[2];
	loc_rma_buf[4 * num_items + 3] = msg[3];

	// clear my ith item
	msg[0] = 0;
	MPI_Accumulate(msg, 4, MPI_INT, rank, 4 * i, 4, MPI_INT,
		       MPI_REPLACE, rma_win);
	MPI_Win_flush_local(rank, rma_win); // todo: necessary?

	num_items++;

      }

    }

    if (!wait)
      break;

    if (!num_items) { // kick the MPI progress engine
      int flag;
      MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD,
		 &flag, MPI_STATUS_IGNORE);
    }

  }

  // process arrived items
  for (int i = 0; i < num_items; i++) {

    int *msg = &loc_rma_buf[4 * i]; // pointer into local rma buffer
    MPI_Aint lb, extent;
    MPI_Type_get_extent(datatype, &lb, &extent);
    items[i] = new unsigned char[msg[3] * extent];
    src_gids[i] = msg[1];
    sizes[i] = msg[3];
    int src_proc = assign->Gid2Proc(src_gids[i]);

    MPI_Recv(items[i], msg[3], datatype, src_proc, my_gid, comm, &status);

  }

  return num_items;

}
//----------------------------------------------------------------------------
//
// flushes RMA sends and receives
//
// note: performs a barrier
//
void Comm::RmaFlushSendRecv() {

  // flush the nonblocking payload sends
  MPI_Status stats[rma_reqs.size()];
  MPI_Waitall(rma_reqs.size(), &rma_reqs[0], stats);
  rma_reqs.clear();

  // clear the RMA buffer
  for (int i = 0; i < DIY_RMA_MAX_ITEMS; i++)
    rma_buf[4 * i] = 0; // mark as available

  MPI_Win_sync(rma_win);
  MPI_Barrier(comm);

}
//----------------------------------------------------------------------------

#endif
